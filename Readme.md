# `Beacon SDK Rescript Bindings`

## Status

⚠️ Work in progress

Only WalletClient currently implemented

## Installation

```console
yarn add rescript-beacon-sdk
```

`rescript-beacon-sdk` should be added to `bs-dependencies` in your
`bsconfig.json`:

```diff
{
  //...
  "bs-dependencies": [
    // ...
+    "rescript-beacon-sdk"
  ],
  //...
}
```

## Authors and acknowledgment
Nomadic-Labs (2021-2023)
* @leoparis89     Lev Kovalski   (2023) / Nomadic-Labs
* @sagotch        Julien Sagot   (2023) / Nomadic-Labs
* @comeh          Corentin Méhat (2023) / Nomadic-Labs (CI/CD)

## License
MIT

## Project status
This project is under development for usage in `umami-mobile` and `umami` (desktop) clients.
